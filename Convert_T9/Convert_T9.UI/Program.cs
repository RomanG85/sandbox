﻿using System;
using Ninject;
using Convert_T9.BLL;

namespace Convert_T9.UI
{
    class Program
    {
        static void Main(string[] args)
        {
            IKernel ninjectKernel = new StandardKernel(new ConfigModule());
            IReplace replace = ninjectKernel.Get<IReplace>();

            Console.WriteLine("Enter Your Text (only english):");
            string text = Console.ReadLine();
            Console.WriteLine(replace.ReplaceLetters(text));
            Console.ReadKey();
        }
    }
}
