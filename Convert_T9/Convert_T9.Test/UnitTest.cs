﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ninject;
using Convert_T9.BLL;

namespace Convert_T9.Test
{
    [TestClass]
    public class BLLTest
    {
        [TestMethod]
        public void Can_replace_letters()
        {
            // Arrange
            IKernel ninjectKernel = new StandardKernel(new ConfigModule());
            IReplace replace = ninjectKernel.Get<IReplace>();
            //string text = "A B C D E F G H I J K L M N O P Q R S T U V W X Y Z";
            string text = "a b c d e f g h i j k l m n o p q r s t u v w x y z @#!&";
            string text1 = "foo bar";


            // Act
            var result = replace.ReplaceLetters(text);
            var result1 = replace.ReplaceLetters(text1);

            // Assert
            Assert.AreEqual("2022022203033033304044044405055055506066066607077077707777080880888090990999099990@#!&", result);
            Assert.AreEqual("3336666660222777", result1);
        }
    }
}
