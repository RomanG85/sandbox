﻿using Ninject.Modules;

namespace Convert_T9.BLL
{
    /// <summary>
    /// IoC for Dependency Injection
    /// </summary>
    public class ConfigModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IReplace>().To<Replace>();
        }
    }
}
