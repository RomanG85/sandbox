﻿namespace Convert_T9.BLL
{
    class Replace : IReplace
    {
        public string ReplaceLetters(string text)
        {
            if(text.ToLower().Contains("a"))
                text = text.Replace("a", "2");

            if (text.ToLower().Contains("b"))
                text = text.Replace("b", "22");

            if (text.ToLower().Contains("c"))
                text = text.Replace("c", "222");

            if (text.ToLower().Contains("d"))
                text = text.Replace("d", "3");

            if (text.ToLower().Contains("e"))
                text = text.Replace("e", "33");

            if (text.ToLower().Contains("f"))
                text = text.Replace("f", "333");

            if (text.ToLower().Contains("g"))
                text = text.Replace("g", "4");

            if (text.ToLower().Contains("h"))
                text = text.Replace("h", "44");

            if (text.ToLower().Contains("i"))
                text = text.Replace("i", "444");

            if (text.ToLower().Contains("j"))
                text = text.Replace("j", "5");

            if (text.ToLower().Contains("k"))
                text = text.Replace("k", "55");

            if (text.ToLower().Contains("l"))
                text = text.Replace("l", "555");

            if (text.ToLower().Contains("m"))
                text = text.Replace("m", "6");

            if (text.ToLower().Contains("n"))
                text = text.Replace("n", "66");

            if (text.ToLower().Contains("o"))
                text = text.Replace("o", "666");

            if (text.ToLower().Contains("p"))
                text = text.Replace("p", "7");

            if (text.ToLower().Contains("q"))
                text = text.Replace("q", "77");

            if (text.ToLower().Contains("r"))
                text = text.Replace("r", "777");

            if (text.ToLower().Contains("s"))
                text = text.Replace("s", "7777");

            if (text.ToLower().Contains("t"))
                text = text.Replace("t", "8");

            if (text.ToLower().Contains("u"))
                text = text.Replace("u", "88");

            if (text.ToLower().Contains("v"))
                text = text.Replace("v", "888");

            if (text.ToLower().Contains("w"))
                text = text.Replace("w", "9");

            if (text.ToLower().Contains("x"))
                text = text.Replace("x", "99");

            if (text.ToLower().Contains("y"))
                text = text.Replace("y", "999");

            if (text.ToLower().Contains("z"))
                text = text.Replace("z", "9999");

            if (text.ToLower().Contains(" "))
                text = text.Replace(" ", "0");

            return text;
        }
    }
}
