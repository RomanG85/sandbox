﻿namespace Convert_T9.BLL
{
    public interface IReplace
    {
        /// <summary>
        /// Replace entered letters on numbers
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        string ReplaceLetters(string text);
    }
}
